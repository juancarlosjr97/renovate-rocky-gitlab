/**
 * Rules shared between JS and TS
 */
const sharedRules = {
  // Makes it easier to distinguish items that'll be auto-fixed
  'prettier/prettier': 'warn',

  // Consistent order to import statements
  'simple-import-sort/imports': 'warn',

  // Using complete words results in more readable code, however, allow some...
  'unicorn/prevent-abbreviations': [
    'warn',
    {
      whitelist: {
        prop: true,
        Prop: true,
        props: true,
        Props: true,
      },
    },
  ],

  // Nested arrow functions are good, no? And this can mess with those. e.g.
  // This would error: () => dispatch => setTimeout(() => dispatch(action), 500)
  'unicorn/consistent-function-scoping': 'off',

  'no-unused-vars': [
    'warn',
    {
      argsIgnorePattern: '^_',
      varsIgnorePattern: '[iI]gnored',
    },
  ],
};

module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:jest/recommended',
    'plugin:cypress/recommended',
    'plugin:unicorn/recommended',
    'prettier',
    'prettier/unicorn',
  ],
  plugins: ['jest', 'prettier', 'simple-import-sort', 'unicorn'],
  rules: {
    ...sharedRules,
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  overrides: [
    {
      files: ['*.js'],
      parser: '',
      extends: [
        'eslint:recommended',
        'plugin:jest/recommended',
        'plugin:cypress/recommended',
        'plugin:unicorn/recommended',
        'prettier',
        'prettier/unicorn',
      ],
      plugins: [],
      rules: {
        ...sharedRules,
      },
    },
  ],
};
