module.exports = {
  // extends: ['config:base'],
  platform: 'gitlab',
  gitAuthor: 'Renovate Bot <juancarlosjr97@gmail.com>',
  baseBranches: ['main'],
  labels: ['dependencies'],
  enabledManagers: ["npm"],
  // enabledManagers: ["python"],
  repositories: ['juancarlosjr97/renovate-rocky-gitlab'],
  updateInternalDeps: true,
  // prConcurrentLimit: 0,
  // prHourlyLimit: 0,
  skipInstalls: false,
  // allowScripts: true,
  force: {
    constraints: {
      node: "16.14.0",
      npm: "8.5.1"
    }
  }
};
